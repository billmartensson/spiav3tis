﻿using UnityEngine;
using System.Collections;

public class airplaneScript : MonoBehaviour {

	Animator airplaneAnimator;

	// Use this for initialization
	void Start () {
		airplaneAnimator = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		int airplaneDirection = 0;
		if(Input.GetKey("a"))
		{
			airplaneDirection = -1;
		}
		if(Input.GetKey("d"))
		{
			airplaneDirection = 1;
		}
		airplaneAnimator.SetInteger("direction", airplaneDirection);

	}
}
